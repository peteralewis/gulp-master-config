// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var basePaths = {
    foundation: './node_modules/foundation-sites/scss',
    motionUI: './node_modules/motion-ui',
    src: './www/assets/src/',
    dest: './www/assets/'
};
var paths = {
    styles: {
        src: basePaths.src + 'scss/**/*.scss',
        dest: basePaths.dest + 'css'
    },
    scripts: {
        src: basePaths.src + 'js/**/*.js',
        dest: basePaths.dest + 'js'
    }
};

// Lint Task
gulp.task('lint', function() {
    return gulp.src(paths.scripts.src)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(paths.styles.src)
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [basePaths.foundation, basePaths.motionUI]
        }))
        .pipe(gulp.dest(paths.styles.dest))
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    var filename = 'main';
    return gulp.src(paths.scripts.src)
        .pipe(concat(filename+'.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(rename(filename+'.min.js'))
        .pipe(uglify())
        // Run again to create a un-minified version
        .pipe(gulp.dest(paths.scripts.dest));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(paths.scripts.src, ['lint', 'scripts']);
    gulp.watch(paths.styles.src, ['sass']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);

// TO DO
// https://github.com/gulpjs/gulp/blob/master/docs/recipes/automate-release-workflow.md
// Creating Sprites: https://www.mikestreety.co.uk/blog/advanced-gulp-file
// Implement Browserify - No, WebPack